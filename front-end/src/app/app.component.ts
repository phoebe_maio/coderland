import { Component, OnInit } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  public data: any;
  private url = 'http://localhost:3000/api/Pusers';

  constructor(
    private http : Http
  ){
    //this.getData();
    this.getUserName();
  }

  ngOnInit() {
  }

  getData() {
    let that = this;
    return this.http.get(this.url)
      .map((res: Response) => res.json())
  }

  getUserName() {
    this.getData().subscribe(data =>{
      this.data = data
      console.log(this.data,'data from api')
    })
  }
}
